//This function and event listener are meant to handle the event of a user checking a checkbox (#consentFlag)

//this variable is a flag we use to determin if we should be setting or clearing the values
var consentFlag = false;

//This function generates the users local time, sets it to cmt (America/Swift_Current), and populates the hidden inputs.
function getTime() {
  //Incidently the 'PST, CMT, EST, are all actually depricated. The new format is to use the one below, it's more reliable as well as it adjusts with daylight savings time.
  //Also using a bunch of options to format the date better so I'm cutting it (bad) less.
  var options = {
    timeZone: "America/Chicago",
    hour12: false,
    month: '2-digit',
    day: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute:'2-digit'
  };
  //toLocaleString is a neat function, lets us make a new Date object and then adjust it to the "en-us" (english US formats) and we pass in our object from above.
  var currentTime = new Date().toLocaleString("en-us", options);
  //Usual jQuery foreach loop...
  $('.timestampDate').each(function(){
    //Slicing the time off the end. Normally I'd say bad approch, make 2 new date objects instead, but since this is static and required, it's safe enough.
    $(this).val(currentTime.substring(0, currentTime.length - 7));
  });
  $('.timestampTime').each(function(){
    //Slicing the date off.
    $(this).val(currentTime.substring(12));
  });

  $('#consentFlag').val('yes');
  $('#privacyFlag').val('yes');
}

//This is our event listener, it's listening for a change in status on the checkbox we gave an ID of consentFlag to.
$('#consentFlag').change(function(){
  //Checking if the consentFlag varaible is false or true. This one because it's using ! is checking "Is this false?"
  if(!consentFlag){
    //Call our function to generate the times / update the fields
    getTime();
    //update our flag since it's true now
    consentFlag = true;
  }else{
    //The other part of the if check, if that came back as true, then the user has clicked again on the checkbox, removing consent. This just then clears the input values.
    $('.timestampDate').each(function(){
      $(this).val('');
    });
    $('.timestampTime').each(function(){
      $(this).val('');
    });
    $('#consentFlag').val('no');
    $('#privacyFlag').val('no');
    //Sets our flag to false again.
    consentFlag = false;
  }
});
